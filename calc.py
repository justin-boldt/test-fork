def funny_add(a, b):
	""" Add 2 numbers, a and b, but add a twist before returning"""
	result = 2*(a + b)
	
	return result

print(funny_add(2, 3))